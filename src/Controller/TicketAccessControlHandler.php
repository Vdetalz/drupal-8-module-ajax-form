<?php
/**
 *
 */

namespace Drupal\ticket\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Determines access to for ticket edit, delete, add pages.
 */
class TicketAccessControlHandler extends EntityAccessControlHandler {
  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *    The currently entity.
   * @param string $operation
   *    The current operation.
   * @param \Drupal\Core\Session\AccountInterface $account
   *    The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultAllowed
   *    The access result.
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'edit':
        return AccessResult::allowedIfHasPermission($account, 'edit ticket');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete ticket');
    }
    return AccessResult::allowed();
  }

  /**
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param array $context
   * @param null $entity_bundle
   *   (optional) The node type.
   *
   * @return \Drupal\Core\Access\AccessResult
   *    The access result.
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add ticket');
  }
}
