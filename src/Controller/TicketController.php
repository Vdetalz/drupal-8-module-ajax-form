<?php

namespace Drupal\ticket\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Unicode;
use Drupal\Component\Utility\Tags;
use Drupal\ticket\Model\TicketModel;
Use \Drupal\file\Entity\File;


/**
 * Returns responses for ticket routes.
 */
class TicketController extends ControllerBase {

  /**
   * A model for querying the database to an bundle - ticket
   *
   * @var \Drupal\ticket\Controller\TicketModel
   */
  protected $model;

  /**
   * TicketController constructor.
   */
  public function __construct() {
    $this->model = new TicketModel();
  }

  /**
   * Displays content links for available tickets.
   *
   * @return array
   *   A render array for a list of the tickets that can be editing or deleting.
   */
  public function ticketListBuilder() {

    $header = [
      ['data' => $this->t('Title'), 'field' => 'title', 'sort' => 'asc'],
      ['data' => $this->t('Date'), 'field' => 'created'],
      ['data' => $this->t('Name')],
      ['data' => $this->t('Location')],
      ['data' => $this->t('Related')],
      ['data' => $this->t('Email')],
      ['data' => $this->t('Action')],
    ];

    $result = $this->model->loadHeader($header);

    $rows = array();
    foreach ($result as $row) {

      $node     = Node::load($row->nid);
      $name     = $node->get('ticket_name')->value;
      $location = $node->get('ticket_location')->value;
      $related  = $node->get('ticket_related')->value;
      $mail     = $node->get('ticket_email')->value;

      $path      = '/admin/config/content/tickets/edit/' . $row->nid;
      $edit      = Url::fromUri('internal:' . $path);
      $edit_link = Link::fromTextAndUrl(t('Edit'), $edit)->toString();

      $path     = '/admin/config/content/tickets/delete/' . $row->nid;
      $del      = Url::fromUri('internal:' . $path);
      $del_link = Link::fromTextAndUrl(t('Delete'), $del)->toString();

      $path       = '/tickets/' . $row->nid;
      $title      = Url::fromUri('internal:' . $path);
      $title_link = Link::fromTextAndUrl($row->title, $title)->toString();

      $mainLink = $this->t('@edit | @delete', [
        '@edit'   => $edit_link,
        '@delete' => $del_link,
      ]);

      $rows[] = [
        ['data' => $title_link],
        ['data' => date('Y-m-d', $row->created)],
        ['data' => $name],
        ['data' => $location],
        ['data' => $related],
        ['data' => $mail],
        ['data' => $mainLink],
      ];
    }

    $build = [
      '#markup' => $this->t('Ticket List'),
    ];

    $build['config_table'] = [
      '#theme'  => 'table',
      '#header' => $header,
      '#rows'   => $rows,
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;
  }

  /**
   * Deleting tickets.
   *
   * @param $ticket_id
   *   The ticket ID.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   RedirectResponse to the ticket list page.
   */
  public function deleteTicket($ticket_id) {
    $this->model->delete($ticket_id);

    drupal_set_message($this->t('Ticket deleted!'));

    return new \Symfony\Component\HttpFoundation\RedirectResponse(\Drupal::url('ticket.admin'));
  }

  /**
   * Displays the ticket archive page.
   *
   * @return array
   *    A render array for a list of the tickets.
   */
  public function archive() {
    $items  = array();
    $result = $this->model->loadTickets();

    foreach ($result as $row) {
      $node       = Node::load($row->nid);
      $path       = '/tickets/' . $row->nid;
      $title      = Url::fromUri('internal:' . $path);
      $title_link = Link::fromTextAndUrl($row->title, $title)->toString();

      $name     = $node->get('ticket_name')->value;
      $phone    = $node->get('ticket_phone')->value;
      $email    = $node->get('ticket_email')->value;
      $location = $node->get('ticket_location')->value;

      $items[] = [
        'title'    => $title_link,
        'name'     => $name,
        'phone'    => $phone,
        'email'    => $email,
        'location' => $location,
      ];
    }

    return [
      '#theme' => 'ticketsarchive',
      '#items' => $items,
    ];
  }

  /**
   * Displays the single ticket.
   *
   * @param $ticket_id
   *   The ticket ID.
   *
   * @return array
   *    A render array for a page of the ticket.
   */
  public function page($ticket_id) {
    $items = array();
    $node  = Node::load($ticket_id);

    $fid       = $node->get('ticket_file')->value;
    $file      = File::load($fid);
    $file_link = file_create_url($file->getFileUri());
    $file_name = $file->getFilename();

    $items['item'] = [
      'title'     => $node->getTitle(),
      'name'      => $node->get('ticket_name')->value,
      'location'  => $node->get('ticket_location')->value,
      'email'     => $node->get('ticket_email')->value,
      'phone'     => $node->get('ticket_phone')->value,
      'file_link' => $file_link,
      'file_name' => $file_name,
      'related'   => $node->get('ticket_related')->value,
    ];

    return [
      '#theme' => 'ticket',
      '#items' => $items,
    ];
  }

  /**
   * Callback for a ticket's form related tickets.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   * @param $field_name
   *   Field name.
   * @param $form_id
   *   Form ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Data in the JSON format.
   */
  public function autocomplete(Request $request, $field_name, $form_id) {
    $matches = array();

    if ($string = $request->query->get('q')) {
      $string = Tags::explode($string);
      $string = Unicode::strtolower(array_pop($string));

      $result = $this->model->loadAutocomplete($string, $form_id);
      $node   = Node::loadMultiple($result);

      foreach ($node as $row) {
        $matches[] = [
          'value' => $field_name . ' - ' . $row->get('ticket_name')->value,
          'label' => $row->get('ticket_name')->value,
        ];
      }
    }
    return new JsonResponse($matches);
  }
}
