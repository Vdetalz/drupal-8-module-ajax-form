<?php

namespace Drupal\ticket\Model;

/**
 * Class TicketModel
 * @package Drupal\ticket\Model
 */
class TicketModel {
  /**
   * Data for ticket.admin route
   *
   * @param array|NULL $header
   *
   * @return \Drupal\Core\Database\StatementInterface|null
   */
  public function loadHeader(array $header = NULL) {

    $db    = \Drupal::database();
    $query = $db->select('node_field_data', 'n');
    $query->condition('type', 'ticket');
    $query->fields('n', ['nid', 'title', 'created',]);

    if ($header) {
      $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
                          ->orderByHeader($header);
      $pager      = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender');
      $result     = $pager->execute();
    }
    else {
      $result = $query->execute();
    }
    return $result;
  }

  /**
   * Deleting ticket
   *
   * @param $ticket_id
   */
  public function delete($ticket_id) {
    $ticket_deleted = \Drupal::database()->delete('node_field_data')
                             ->condition('nid', $ticket_id)
                             ->execute();
  }

  /**
   * Data for ticket.archive and ticket.page routes.
   *
   * @param null $count
   *   The count tickets.
   *
   * @return \Drupal\Core\Database\StatementInterface|null
   */
  public function loadTickets($count = NULL) {
    $db    = \Drupal::database();
    $query = $db->select('node_field_data', 'n');
    $query->fields('n', ['nid', 'title']);
    $query->condition('type', 'ticket');
    $query->condition('status', 1);

    if ($count) {
      $query->range(0, $count);
    }

    $query->orderBy('created', 'DESC');
    $result = $query->execute();

    return $result;
  }

  /**
   * Data for ticket.autocomplete route.
   * Calling in TicketController::autocomplete.
   *
   * @param $string
   *
   * @return array|int
   */
  public function loadAutocomplete($string, $form_id = NULL) {
    $query = \Drupal::entityQuery('node');
    $query->condition('type', 'ticket');
    $query->condition('status', 1);

    if ($form_id) {
      $query->condition('nid', $form_id, 'NOT IN');
    }

    $query->condition('ticket_name', '%' . db_like($string) . '%', 'LIKE');
    $result = $query->execute();

    return $result;
  }
}