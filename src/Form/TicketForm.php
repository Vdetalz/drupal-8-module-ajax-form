<?php
namespace Drupal\ticket\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax;
use Drupal\Core\Ajax\HtmlCommand;
Use \Drupal\file\Entity\File;


/**
 * Class TicketForm
 * @package Drupal\ticket\Form
 *
 * Contains a form for view mode of a node type ticket during preview or add.
 */
class TicketForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ticket_add_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param null $ticket_id
   *   The node type ticket being previews
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $ticket_id = NULL) {

    if ($ticket_id != NULL) {
      $ticket = $this->getEditValue($ticket_id);
    }
    else {
      $ticket = NULL;
    }

    $form['title'] = [
      '#type'          => 'textfield',
      '#attributes'    => [
        'placeholder' => $this->t('Insert Title'),
      ],
      '#title'         => $this->t('Ticket Title'),
      '#default_value' => $ticket ? $ticket['title'] : '',
      '#required'      => TRUE,
    ];

    $form['name'] = [
      '#title'         => $this->t('Name'),
      '#attributes'    => [
        'placeholder' => $this->t('Insert Your Name'),
      ],
      '#default_value' => $ticket ? $ticket['name'] : '',
      '#type'          => 'textfield',
      '#required'      => TRUE,
      '#ajax'          => [
        'callback' => '::validateNameAjax',
        'event'    => 'change',
        'progress' => [
          'type'    => 'throbber',
          'message' => t('Verifying name...'),
        ],
      ],
      '#suffix'        => '<div class="name-validate"></div>',
    ];

    $form['email'] = [
      '#title'         => $this->t('Email'),
      '#attributes'    => [
        'placeholder' => $this->t('Insert Your email'),
      ],
      '#default_value' => $ticket ? $ticket['email'] : '',
      '#type'          => 'email',
      '#required'      => TRUE,
      '#ajax'          => [
        'callback' => '::validateEmailAjax',
        'event'    => 'change',
        'progress' => [
          'type'    => 'throbber',
          'message' => t('Verifying email...'),
        ],
      ],
      '#suffix'        => '<div class="email-validate"></div>',
    ];

    $form['phone'] = [
      '#title'         => $this->t('Phone'),
      '#attributes'    => [
        'placeholder' => $this->t('Phone number like  (999) 999-9999'),
      ],
      '#default_value' => $ticket ? $ticket['phone'] : '',
      '#type'          => 'tel',
      '#required'      => TRUE,
      '#pattern'       => '\([0-9]{3}\)[0-9]{3}[-][0-9]{4}',
      '#ajax'          => [
        'callback' => '::validatePhoneAjax',
        'event'    => 'change',
        'progress' => [
          'type'    => 'throbber',
          'message' => t('Verifying phone...'),
        ],
      ],
      '#suffix'        => '<div class="phone-validate"></div>',

    ];

    $form['location'] = [
      '#title'         => $this->t('Location'),
      '#description'   => $this->t('Your Location'),
      '#default_value' => $ticket ? $ticket['location'] : '',
      '#type'          => 'textarea',
      '#required'      => TRUE,
      '#ajax'          => [
        'callback' => '::validateLocationAjax',
        'event'    => 'change',
        'progress' => [
          'type'    => 'throbber',
          'message' => t('Searching locations...'),
        ],
      ],
      '#suffix'        => '<div class="location-validate"></div>',
    ];

    $form['file'] = [
      '#title'              => $this->t('File'),
      '#description'        => $this->t('Upload a .pdf File(max size 3mb)'),
      '#type'               => 'managed_file',
      //'#required'    => TRUE,
      '#progress_indicator' => 'bar',
      '#progress_message'   => 'Uploading...',
      '#size'               => 8,
      '#upload_location'    => 'public://files/',
      //'#default_value' => $ticket ? $ticket['file'] : '',
      '#upload_validators'  => [
        'file_validate_extensions' => ['pdf'],
        'file_validate_size'       => [8 * 1024 * 1024],
      ],
    ];

    $form['related'] = [
      '#title'                         => $this->t('Related Ticket'),
      '#attributes'                    => [
        'placeholder' => $this->t('Enter Related Ticket'),
      ],
      '#default_value'                 => $ticket ? $ticket['related'] : '',
      '#autocomplete_route_name'       => 'ticket.autocomplete',
      '#autocomplete_route_parameters' => [
        'field_name' => 'related',
        'form_id'    => $ticket ? $ticket['nid'] : '',
      ],
      '#type'                          => 'textfield',
    ];

    $form['system_messages'] = [
      '#markup' => '<div id="form-system-messages"></div>',
      '#weight' => -100,
    ];

    $form['actions']['#type']  = 'actions';
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $ticket ? $this->t('Update Ticket') : $this->t('Add Ticket'),
      '#button_type' => 'primary',
    ];

    $form['#attached']['library'][] = 'ticket/googleplaces';
    $form['#attached']['library'][] = 'ticket/ticketlib';

    if ($ticket) {
      $form['id'] = [
        '#type'  => 'value',
        '#value' => $ticket['nid'],
      ];
    }
    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function validateNameAjax(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    if (mb_strlen($form_state->getValue('name'), 'utf-8') <= 3) {
      $response->addCommand(new HtmlCommand('.name-validate', t('Name is too short.')));
    }
    else {
      $response->addCommand(new HtmlCommand('.name-validate', ''));
    }
    return $response;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function validateEmailAjax(array &$form, FormStateInterface $form_state) {
    $response       = new AjaxResponse();
    $email          = trim($form_state->getValue('email'));
    $emailValidator = \Drupal::service('email.validator')->isValid($email);

    if (!$emailValidator) {
      $response->addCommand(new HtmlCommand('.email-validate', t('Email not correct.')));
    }
    else {
      $response->addCommand(new HtmlCommand('.email-validate', ''));
    }
    return $response;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function validatePhoneAjax(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $phone    = $form_state->getValue('phone');
    $pattern  = '/\([0-9]{3}\)[0-9]{3}[-][0-9]{4}$/i';

    if (!preg_match($pattern, $phone)) {
      $response->addCommand(new HtmlCommand('.phone-validate', t('Telephone is not correct.')));
    }
    else {
      $response->addCommand(new HtmlCommand('.phone-validate', ''));
    }
    return $response;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function validateLocationAjax(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    if (mb_strlen($form_state->getValue('location'), 'utf-8') <= 3) {
      $response->addCommand(new HtmlCommand('.location-validate', t('Location is too short.')));
    }
    else {
      $response->addCommand(new HtmlCommand('.location-validate', ''));
    }
    return $response;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function validateRelatedAjax(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    if (mb_strlen($form_state->getValue('related'), 'utf-8') <= 3) {
      $response->addCommand(new HtmlCommand('.related-validate', t('Related is too short.')));
    }
    else {
      $response->addCommand(new HtmlCommand('.related-validate', ''));
    }
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('file') == NULL) {
      $form_state->setErrorByName('file', $this->t('File.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $title     = $form_state->getValue('title');
    $name      = $form_state->getValue('name');
    $email     = $form_state->getValue('email');
    $phone     = $form_state->getValue('phone');
    $location  = $form_state->getValue('location');
    $related   = $form_state->getValue('related');
    $file      = $form_state->getValue('file');
    $fid       = $file[0];
    $file_save = File::load($fid);
    $file_save->setPermanent();
    $file_save->save();
    $val = $form_state->getValue('id');

    /*Edit Ticket*/
    if (isset($val)) {
      $node = Node::load($val);
      $node->set('title', $title);
      $node->set('ticket_name', $name);
      $node->set('ticket_email', $email);
      $node->set('ticket_phone', $phone);
      $node->set('ticket_location', $location);
      $node->set('ticket_related', $related);
      $node->set('ticket_file', $fid);
      $node->save();
      drupal_set_message(t('Ticket saved!'));
    }
    /*Add new data.*/
    else {
      $node = Node::create([
        'type'            => 'ticket',
        'title'           => $title,
        'ticket_name'     => $name,
        'ticket_email'    => $email,
        'ticket_phone'    => $phone,
        'ticket_location' => $location,
        'ticket_related'  => $related,
        'ticket_file'     => $fid,
      ]);
      $node->save();
      drupal_set_message(t('Ticket added!'));
    }
    $form_state->setRedirect('ticket.admin');
    return;
  }

  /**
   * Check if such ticket exists.
   *
   * @param $id
   *   Form ID corresponding to the ticket nid.
   *
   * @return array
   *   Data about an existing ticket, if it exists.
   */
  public function getEditValue($id) {

    $ticket = [];
    if (isset($id)) {
      $node               = Node::load($id);
      $ticket['nid']      = $id;
      $ticket['title']    = $node->get('title')->value;
      $ticket['name']     = $node->get('ticket_name')->value;
      $ticket['email']    = $node->get('ticket_email')->value;
      $ticket['phone']    = $node->get('ticket_phone')->value;
      $ticket['location'] = $node->get('ticket_location')->value;
      $ticket['related']  = $node->get('ticket_related')->value;
      $ticket['file']     = $node->get('ticket_file')->value;
    }
    return $ticket;
  }
}